---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.15.0
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

# Python RedPitaya Control

Some widgets and code for acquiring data and controlling the RedPitaya using the PyRPL library. 

You will need to install PyRPL: 

https://pyrpl.readthedocs.io/en/latest/user_guide/installation/pyrpl_installation.html#installation-from-source

From an Ananconda prompt, run the following, ideally in a separate environment (I called my pyrpl3.8 and made it with. `conda create -n pyrpl3.8`:

```
conda install python=3.9 numpy=1.19
conda install numpy scipy paramiko pandas nose pip pyqt qtpy pyqtgraph pyyaml
conda install ipywidgets
conda install notebook=6.0
pip install pyrpl quamash jupyter-ui-poll
```

Likely due to changes in the threading interface of the ipykernel, the data acquisition functions from pyrpl do not work with the latest versions of python (see ?https://github.com/lneuhaus/pyrpl/issues/500). To solve this, you will want to install pyrpl from sources, using the git repository. 

* Step 1: Clone the pyrpl sources into the folder on your computer where you keep code repositories: `git clone https://github.com/lneuhaus/pyrpl.git`
* Step 2: Switch to the python3-only branch: `git checkout python3-only`
* Step 3: Install pyrpl from that codebase: `pip install .`

The last step will ensure that the libraries in your environment are downgraded to the appropriate versions to make things work. 

Probably one could pre-downgrade libraries ahead of time and then maybe things would work directly with the pip version of pyrpl, but I have not had time to test that yet...

```python
HOSTNAME = "rp-f07198.local" # hostname of the red pitaya
HOSTNAME = "rp-f083c0.local" # hostname of the red pitaya
HOSTNAME = "rp-f0839f.local" # hostname of the red pitaya
HOSTNAME = "rp-f071a9.local" # hostname of the red pitaya
folder = "."  # relative folder where files are saved
```

```python hide_input=false
import warnings
warnings.filterwarnings('ignore')

import sys
import os
from pyrpl import Pyrpl
import numpy as np
import time

import IPython
import ipywidgets as widgets

from bokeh.plotting import figure, show
from bokeh.io import output_notebook, push_notebook
from bokeh.layouts import column, row
from bokeh.models import ColumnDataSource, Toggle, Range1d

from scipy.signal import welch

from datetime import datetime

# For interacting with gui events, thank god!
# https://stackoverflow.com/questions/70436504/problem-with-event-loops-gui-qt5-and-ipywidgets-in-a-jupyter-notebook/76887842#76887842
from jupyter_ui_poll import ui_events

# Red Pitaya Stuff

p = Pyrpl('test', hostname=HOSTNAME, gui=False)
r = p.rp
s = r.scope

# For Bokeh
output_notebook()
```

```python hide_input=false
# The trace acquisition
def get_traces(decimation = 1024):
    s.decimation = decimation
    #     fut = s._start_acquisition()
    #     while True:
    #         pyrpl.async_sleep(s.duration/10)
    #         ready = s.curve_ready()
    #         if ready: 
    #             break
    #     ch1,ch2 = s._get_curve()
    # With right combination of library versions, single() now works
    # (see https://github.com/lneuhaus/pyrpl/issues/500)
    ch1,ch2 = s.single()
    t = s.times
    return ch1,ch2,t
```

```python
# Some good default settings
s.input1 = 'in1'
s.input2 = 'in2'
s.decimation = 1024 # or s.duration =0.01
s.average = True
s.trigger_source = 'immediately'
get_traces()
```

# Generator Control

```python
sg_ch1 = r.asg0
sg_ch1.output_direct = 'out1'
sg_ch2 = r.asg1
sg_ch2.output_direct = 'out2'
```

```python
sg_ch1.setup(frequency=9e5, amplitude=0.1, waveform='sin', trigger_source='immediately')
sg_ch2.setup(frequency=1e6, amplitude=0.1, waveform='cos', trigger_source='immediately')
```

# Spectrum Analyser

```python hide_input=true
# Some good default settings
s.input1 = 'in1'
s.input2 = 'in2'
s.decimation = 1024 # or s.duration =0.01
s.average = True
s.trigger_source = 'immediately'

# The traces we download
ch1 = []
ch2 = []
t = []

# The calculated spectral densities
ch1_psd = []
ch2_psd = []
f = []

# Each PSD trace is 8193 points long
n_psd = 8193
nmax  =  100
ch1_psd_array = np.zeros([nmax,n_psd])
ch2_psd_array = np.zeros([nmax,n_psd])

# For keeping track of how many we've filled and where the next goes
ind_next = 0
n_filled = 0


def update_display():
    global ch1,ch2,t,f,ch1_psd,ch2_psd,last_trace_time,ind_next,n_filled
    ch1,ch2,t = get_traces(decimation=decimation.value)
    f, ch1_psd_trace = welch(ch1,1/t[1],nperseg=len(ch1))
    f, ch2_psd_trace = welch(ch2,1/t[1],nperseg=len(ch2))
    
    if enable_average.value:
        # ind_next: the running index
        # n_filled: the number of traces taken since the last reset
        
        ch1_psd_array[ind_next,:] = ch1_psd_trace
        ch2_psd_array[ind_next,:] = ch2_psd_trace
        
        if n_filled  == 0:
            ch1_psd = ch1_psd_trace
            ch2_psd = ch2_psd_trace
        elif n_filled < nmax:
            ch1_psd = (ch1_psd * n_filled + ch1_psd_trace) / (n_filled+1) 
            ch2_psd = (ch2_psd * n_filled  + ch2_psd_trace) / (n_filled+1)
        else:
            # this should work because of numpy array index wrapping
            ch1_psd += ch1_psd_trace / nmax - ch1_psd_array[ind_next-nmax]/nmax
            ch2_psd += ch2_psd_trace / nmax - ch2_psd_array[ind_next-nmax]/nmax  
        ind_next += 1
        if ind_next == 100:
            ind_next = 0
        if n_filled < 100:
            n_filled += 1
            averaged_num.value = "%d" % n_filled          
    else:
        ch1_psd = ch1_psd_trace
        ch2_psd = ch2_psd_trace
        n_filled = 0
        ind_next = 0
        if averaged_num.value != "1":
            averaged_num.value = "1"
    
    source1.data = dict(x=f, y=ch1_psd)
    source2.data = dict(x=f, y=ch2_psd)

source1 = ColumnDataSource()
source2 = ColumnDataSource()

p1 = figure(title="Ch1", height=300, y_axis_type="log",
            width=900)
p1.yaxis.axis_label = 'Voltage PSD dB(V^2/Hz)'

p2 = figure(title="Ch2", height=300, y_axis_type="log",
            width=900)
p2.xaxis.axis_label = 'Frequency (Hz)'
p2.yaxis.axis_label = 'Voltage PSD dB(V^2/Hz)'

p1.line('x', 'y', source=source1)
p2.line('x', 'y', source=source2)

stop_button = widgets.ToggleButton(description='Stop')
pause_button = widgets.ToggleButton(description='Pause')

def reset_average(w):
    global n_filled, ind_next
    n_filled = 0
    ind_next = 0

# For the decimation GUI
options = []
for d,t in zip(s.decimation_options, s.duration_options):
    fmax = (1/t*n_psd/1e3)
    if t < 1e-3:
        t*= 1e6
        u = "us"
    elif t<1:
        t*= 1e3
        u = "ms"
    else: 
        u = "s"
    options.append((str(d) + ", " "{0:.3g}".format(t) + " " + u + ", %.1f kHz" % fmax,d))
options

def update_freq(w):
    s.decimation = decimation.value
    trace_info.value = "Frequency resolution: %.2f Hz &nbsp; &nbsp; &nbsp; &nbsp;" % (1/s.duration)
    trace_info.value += "Max frequency: %.3f kHz" % (1/s.duration*n_psd/1e3)

decimation = widgets.Dropdown(description="Decimation:", 
                              options=options)
decimation.observe(reset_average)
decimation.observe(update_freq)
trace_info = widgets.HTML()

style = {'description_width': 'initial'}
enable_average = widgets.Checkbox(description="Enable averaging (max 100 traces)", style=style)
averaged_num = widgets.HTML(value="1", description="Number averaged:", style=style)
reset_button = widgets.Button(description="Reset averaging")
reset_button.on_click(reset_average)

def save_data(w):
    fmt = f"PSD_%Y-%m-%d-%H_%M_%S.dat"
    outname = folder + "/" + datetime.now().strftime(fmt)
    np.savetxt(outname, np.array([f,ch1_psd,ch2_psd]).T)
    filename.value = outname
    
save_button = widgets.Button(description='Save data')
save_button.on_click(save_data)
filename = widgets.HTML(description="Last filename: ",style=style)
filename.value = "(none)"

update_display()
update_freq(0)
target = show(column(p1,p2), notebook_handle=True)
rows = []
rows.append(widgets.HBox([decimation,trace_info]))
rows.append(widgets.HBox([enable_average, averaged_num, reset_button]))
rows.append(widgets.HBox([stop_button,pause_button,save_button, filename]))
display(widgets.VBox(rows))

while True:
    with ui_events() as ui_poll:
        ui_poll(10)
    if not pause_button.value:
        update_display()
        push_notebook(handle=target)
    if stop_button.value:
        break
    time.sleep(0.1)

print("Live view done")
```

# Single channel Spectrum Analyzer with Zoom FFT


OK, let's see if I can get the zoom FFT thing going. 


Now, to get that into a trace, I need to set the scope to capture it. Since there is only one scope, and it has only two channels, the zoom FFT will never work with two channels. So I can better make a new code that just works with one channel, and give that one the FFT zoom option. That would be quite reasonable. 

```python
iq = r.iq2
iq.setup(input = 'in1', #Meaning IN1 is used
        frequency=9e5,
        bandwidth=[1e3]*2, # 4th order filter TODO let user select
        gain=0,
        phase=0,
        acbandwidth=0,
        quadrature_factor = 1, # Supremely important it does not work without does for some reason
        amplitude=1,
        output_direct='off',
        output_signal='quadrature')
s.input1 = 'iq2'
s.input2 = 'iq2_2'
s.trigger_source = 'immediately'
s._start_acquisition()
time.sleep(s.duration)
I,Q = s._get_curve()
I -= np.mean(I)
Q -= np.mean(Q)

psd = np.abs(np.fft.fft(I+1j*Q))**2
psd = np.fft.fftshift(psd)
ind = np.array(range(len(psd)))
p = figure(y_axis_type='log', height=400); p.line(ind,psd); show(p)
```

```python

```

```python
I
```

```python hide_input=true
iq = r.iq2
iq.setup(input = 'in1', #Meaning IN1 is used
        frequency=1e6,
        bandwidth=[1e5]*2, # 4th order filter TODO let user select
        gain=0,
        phase=0,
        acbandwidth=0,
        quadrature_factor = 1, # Supremely important it does not work without does for some reason
        amplitude=1,
        output_direct='off',
        output_signal='quadrature')
s.input2 = 'iq2_2'

# The traces we download
ch1 = []
t = []

# The calculated spectral densities
ch1_psd = []
f = []

# Each PSD trace is 8193 points long
n_psd = 8193
nmax  =  100
ch1_psd_array = np.zeros([nmax,n_psd])

# For keeping track of how many we've filled and where the next goes
ind_next = 0
n_filled = 0

zoom_checkbox = 
def zoom_checkbox_callback(c):
    if 

def update_display_zoom():
    
    

def update_display_no_zoom():
    global ch1,t,f,ch1_psd,last_trace_time,ind_next,n_filled
    ch1,ch2,t = get_traces(decimation=decimation.value)
    f, ch1_psd_trace = welch(ch1,1/t[1],nperseg=len(ch1))
    if enable_average.value:
        # ind_next: the running index
        # n_filled: the number of traces taken since the last reset
        ch1_psd_array[ind_next,:] = ch1_psd_trace
        if n_filled  == 0:
            ch1_psd = ch1_psd_trace
        elif n_filled < nmax:
            ch1_psd = (ch1_psd * n_filled + ch1_psd_trace) / (n_filled+1) 
        else:
            # this should work because of numpy array index wrapping
            ch1_psd += ch1_psd_trace / nmax - ch1_psd_array[ind_next-nmax]/nmax
        ind_next += 1
        if ind_next == 100:
            ind_next = 0
        if n_filled < 100:
            n_filled += 1
            averaged_num.value = "%d" % n_filled          
    else:
        ch1_psd = ch1_psd_trace
        n_filled = 0
        ind_next = 0
        if averaged_num.value != "1":
            averaged_num.value = "1"
    source1.data = dict(x=f, y=ch1_psd)

source1 = ColumnDataSource()

p1 = figure(title="Ch1", height=300, y_axis_type="log",
            width=900)
p1.yaxis.axis_label = 'Voltage PSD dB(V^2/Hz)'

p1.line('x', 'y', source=source1)

stop_button = widgets.ToggleButton(description='Stop')
pause_button = widgets.ToggleButton(description='Pause')

def reset_average(w):
    global n_filled, ind_next
    n_filled = 0
    ind_next = 0

# For the decimation GUI
options = []
for d,t in zip(s.decimation_options, s.duration_options):
    fmax = (1/t*n_psd/1e3)
    if t < 1e-3:
        t*= 1e6
        u = "us"
    elif t<1:
        t*= 1e3
        u = "ms"
    else: 
        u = "s"
    options.append((str(d) + ", " "{0:.3g}".format(t) + " " + u + ", %.1f kHz" % fmax,d))
options

def update_freq(w):
    s.decimation = decimation.value
    trace_info.value = "Frequency resolution: %.2f Hz &nbsp; &nbsp; &nbsp; &nbsp;" % (1/s.duration)
    trace_info.value += "Max frequency: %.3f kHz" % (1/s.duration*n_psd/1e3)

decimation = widgets.Dropdown(description="Decimation:", 
                              options=options)
decimation.observe(reset_average)
decimation.observe(update_freq)
trace_info = widgets.HTML()

style = {'description_width': 'initial'}
enable_average = widgets.Checkbox(description="Enable averaging (max 100 traces)", style=style)
averaged_num = widgets.HTML(value="1", description="Number averaged:", style=style)
reset_button = widgets.Button(description="Reset averaging")
reset_button.on_click(reset_average)

def save_data(w):
    fmt = f"PSD_%Y-%m-%d-%H_%M_%S.dat"
    outname = folder + "/" + datetime.now().strftime(fmt)
    np.savetxt(outname, np.array([f,ch1_psd,ch2_psd]).T)
    filename.value = outname
    
save_button = widgets.Button(description='Save data')
save_button.on_click(save_data)
filename = widgets.HTML(description="Last filename: ",style=style)
filename.value = "(none)"

update_display()
update_freq(0)
target = show(column(p1,p2), notebook_handle=True)
rows = []
rows.append(widgets.HBox([decimation,trace_info]))
rows.append(widgets.HBox([enable_average, averaged_num, reset_button]))
rows.append(widgets.HBox([stop_button,pause_button,save_button, filename]))
display(widgets.VBox(rows))

while True:
    with ui_events() as ui_poll:
        ui_poll(10)
    if not pause_button.value:
        update_display()
        push_notebook(handle=target)
    if stop_button.value:
        break
    time.sleep(0.1)

print("Live view done")
```

# Oscilloscope

```python hide_input=true
# The traces we download
ch1 = []
ch2 = []
t = []

# For keeping track of how many we've filled and where the next goes
ind_next = 0
n_filled = 0

def update_display():
    global ch1,ch2,t
    global ind_next,n_filled
    ch1,ch2,t = get_traces(decimation=decimation.value)
    source1.data = dict(x=t, y=ch1)
    source2.data = dict(x=t, y=ch2)

source1 = ColumnDataSource()
source2 = ColumnDataSource()

range1 = Range1d()
range2 = Range1d()
p1 = figure(title="Ch1", height=300, width=900,x_range=range1)
p1.yaxis.axis_label = 'Voltage (V)'

p2 = figure(title="Ch2", height=300, width=900,x_range=range2)
p2.xaxis.axis_label = 'Time (s)'
p2.yaxis.axis_label = 'Voltage (V)'

p1.line('x', 'y', source=source1)
p2.line('x', 'y', source=source2)

stop_button = widgets.ToggleButton(description='Stop')
pause_button = widgets.ToggleButton(description='Pause')

def reset_average(w):
    global n_filled, ind_next
    n_filled = 0
    ind_next = 0

# For the decimation GUI
options = []
try:
    dur = s.duration_options
except:
    dur = s.durations
        
for d,t in zip(s.decimation_options, dur):
    if t < 1e-3:
        t*= 1e6
        u = "us"
    elif t<1:
        t*= 1e3
        u = "ms"
    else: 
        u = "s"
    options.append((str(d) + ", " "{0:.3g}".format(t) + " " + u,d))

def update_time(w):
    s.decimation = decimation.value
    trace_info.value = "Time resolution: %.3e s &nbsp; &nbsp; &nbsp; &nbsp;" % (s.duration/len(ch1))
    trace_info.value += "Acquisition time: %.3e s" % (s.duration)
    update_display()
    set_zoom([])

decimation = widgets.Dropdown(description="Decimation:", 
                              options=options)
decimation.observe(reset_average)
decimation.observe(update_time)
trace_info = widgets.HTML()

def set_trigger(w):
    s.trigger_source = trigger_source.value

trigger_source = widgets.Dropdown(description="Trigger source:", options=s.trigger_sources)
trigger_source.observe(set_trigger)
trigger_source.value = s.trigger_source

trigger_threshold = widgets.FloatText(description="Trigger threshold:")
def set_trigger_threshold(w):
    s.threshold = trigger_threshold.value
trigger_threshold.value = s.threshold

style = {'description_width': 'initial'}
enable_average = widgets.Checkbox(description="Enable averaging (max 100 traces)", style=style)
averaged_num = widgets.HTML(value="1", description="Number averaged:", style=style)
reset_button = widgets.Button(description="Reset averaging")
reset_button.on_click(reset_average)

def save_data(w):
    fmt = f"Scope_%Y-%m-%d-%H_%M_%S.dat"
    outname = folder + "/" + datetime.now().strftime(fmt)
    np.savetxt(outname, np.array([t,ch1,ch2]).T)
    filename.value = outname

zoom = widgets.FloatSlider(description="Zoom", min=0.01,max=1,step=0.01,value=0.1)
def set_zoom(w):
    start = (center.value - zoom.value) * np.max(t)
    end = (center.value + zoom.value) * np.max(t) 
    range1.start = start
    range2.start = start
    range1.end = end
    range2.end = end
zoom.observe(set_zoom)

center = widgets.FloatSlider(description="Center", min=-1,max=1,step=0.01,value=0.0)
center.observe(set_zoom)

save_button = widgets.Button(description='Save data')
save_button.on_click(save_data)
filename = widgets.HTML(description="Last filename: ",style=style)
filename.value = "(none)"

update_display()
update_time(0)
target = show(column(p1,p2), notebook_handle=True)
rows = []
rows.append(widgets.HBox([decimation,trace_info]))
rows.append(widgets.HBox([zoom,center]))
rows.append(widgets.HBox([trigger_source,trigger_threshold]))
#rows.append(widgets.HBox([enable_average, averaged_num, reset_button]))
rows.append(widgets.HBox([stop_button,pause_button,save_button, filename]))
display(widgets.VBox(rows))

while True:
    with ui_events() as ui_poll:
        ui_poll(10)
    if not pause_button.value:
        update_display()
        push_notebook(handle=target)
    if stop_button.value:
        break
    time.sleep(0.1)

print("Live view done")
```
