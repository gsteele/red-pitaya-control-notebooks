from pyrpl import Pyrpl
from pyrpl.async_utils import sleep as pyrpl_sleep

import sys
import os
import numpy as np
import time

# +
import ipywidgets as widgets

from bokeh.plotting import figure, show
from bokeh.io import output_notebook, push_notebook
from bokeh.layouts import column, row, grid
from bokeh.models import ColumnDataSource, Toggle, Range1d, LinearAxis, DataRange1d
from bokeh.models import PrintfTickFormatter

from scipy.signal import periodogram

from datetime import datetime
# -

import numpy as np
from bokeh.plotting import figure, show
from bokeh.io import output_notebook, push_notebook
# output_notebook()
def bokeh_plot(y,x=None):
    if x is None:
        x = np.array(range(len(y)))
    p = figure(height=400)
    p.line(x,y)
    show(p)
def bokeh_log_plot(y,x=None):
    if x is None:
        x = np.array(range(len(y)))
    p = figure(height=400,y_axis_type='log')
    p.line(x,y)
    show(p)   


p = None
r = None
s = None
folder = None

# For debugging and developing the code
DEBUG = False

# DEBUG = True

plot_height = 300
bokeh_sleep = 0.2

# +
# For interacting with gui events, thank god!
# https://stackoverflow.com/questions/70436504/
# problem-with-event-loops-gui-qt5-and-ipywidgets-in-a-jupyter-notebook/76887842#76887842
from jupyter_ui_poll import ui_events

def initialize(hostname, data_folder):
    global p,r,s, folder
    folder = data_folder
    p = Pyrpl('test', hostname=hostname, gui=False)
    r = p.rp
    s = r.scope
    if not s:
        print("Error connecting to the Red Pitaya")

    # Some good default settings
    s.input1 = 'in1'
    s.input2 = 'in2'
    s.decimation = 1024 # or s.duration =0.01
    s.average = True
    s.trigger_source = 'immediately'
    
    # For Bokeh
    output_notebook()

    
# For the decimation GUI
from matplotlib.ticker import EngFormatter

def decimination_dropdown_options():
    decimation_options = []
    fmt1 = EngFormatter()
    fmt1.places = 1
    fmt1.unit = "s"
    fmt2 = EngFormatter()
    fmt2.places = 1
    fmt2.unit = "Sa/s"
    for d,t in zip(s.decimation_options, s.duration_options):
        fmax = (1/t*s.data_length)
        option_text = "%d" % d # Decimation
        option_text += " / " + fmt1(t) # length of trace in time
        option_text += " / " + fmt2(fmax) #
        decimation_options.append((option_text,d))
    return decimation_options

def refresh_bokeh():
    output_notebook()
    
# An attempt at async acquisition
#
# Hope was that it would allow UI interactions between traces
# That did not work
# Furthermore, any attempt to use single_async() causes 
# everything to just hang every so often requiring a kernel
# restart. Probably better just to do the threading myself. 
# Problem is that the threading in pyrpl is all mixed up into 
# Quamash and QT event loops...
#
# def get_traces(decimation = 1024):
#     s.decimation = decimation
#     res = s.single_async()
#     while True:
#         pyrpl_sleep(s.duration/10) # need to sleep a bit for curve_ready() to be accurate
#         ready = s.curve_ready()
#         if ready:
#             break
#         # OK, this breaks things. I think I need to go look 
#         # at what is happening inside pyrpl_sleep...or learn
#         # async programming...
#         #with ui_events() as ui_poll:
#             #ui_poll(10) # this one is causing problems!
#     ch1,ch2 = res.result()
#     t = s.times
#     return ch1,ch2,t

# The trace acquisition
def get_traces():
    if False: # see if this is what is causing the crashes...
        ch1,ch2 = s.single()
        t = s.times
        # A bug in the first two point?
        ch1[0] = ch1[1] = ch1[2]
        ch2[0] = ch2[1] = ch2[2]
    else:
        try: # python3-only
            s._start_trace_acquisition()
        except: # pip verion,0.9.3
            s._start_acquisition()
        time.sleep(s.duration+10e-3)
        try:
            ch1,ch2 = s._get_trace()
        except:
            ch1,ch2 = s._get_curve()
        t = s.times
    return ch1,ch2,t


# -

if DEBUG: 
    HOSTNAME = "rp-f071a9.local" # hostname of the red pitaya
    initialize(HOSTNAME, '.')


# # Spectrum analyzer

def run_spectrum_analyzer():
    global folder, s, r
    # The traces we download
    ch1 = []
    ch2 = []
    t = []

    # The calculated spectral densities
    ch1_psd = []
    ch2_psd = []
    f = []

    # Each PSD trace is 8193 points long
    n_psd = 8193
    nmax  =  100
    ch1_psd_array = np.zeros([nmax,n_psd])
    ch2_psd_array = np.zeros([nmax,n_psd])

    # For keeping track of how many we've filled and where the next goes
    ind_next = 0
    n_filled = 0

    # Trigger needs to be "immediate"
    s.trigger_source = 'immediately'
    
    def update_display():
        global ch1,ch2,t,f,ch1_psd,ch2_psd,last_trace_time,ind_next,n_filled

        t0 = time.time()
        s.decimation = decimation.value
        ch1,ch2,t = get_traces()
        f, ch1_psd_trace = periodogram(ch1,1/t[1])
        f, ch2_psd_trace = periodogram(ch2,1/t[1])
        if enable_average.value:
            # ind_next: the running index
            # n_filled: the number of traces taken since the last reset

            ch1_psd_array[ind_next,:] = ch1_psd_trace
            ch2_psd_array[ind_next,:] = ch2_psd_trace

            if n_filled  == 0:
                ch1_psd = ch1_psd_trace
                ch2_psd = ch2_psd_trace
            elif n_filled < nmax:
                ch1_psd = (ch1_psd * n_filled + ch1_psd_trace) / (n_filled+1) 
                ch2_psd = (ch2_psd * n_filled + ch2_psd_trace) / (n_filled+1) 
            else:
                # this should work because of numpy array index wrapping
                ch1_psd += ch1_psd_trace / nmax - ch1_psd_array[ind_next-nmax]/nmax
                ch2_psd += ch2_psd_trace / nmax - ch2_psd_array[ind_next-nmax]/nmax  
            ind_next += 1
            if ind_next == 100:
                ind_next = 0
            if n_filled < 100:
                n_filled += 1
                averaged_num.value = "%d" % n_filled          
        else:
            ch1_psd = ch1_psd_trace
            ch2_psd = ch2_psd_trace
            n_filled = 0
            ind_next = 0
            if averaged_num.value != "1":
                averaged_num.value = "1"
        l1.data_source.data = dict(x=f, y=ch1_psd)
        l2.data_source.data = dict(x=f, y=ch2_psd)

    ##########
    ## Make the Plots
    ##########
    
    p1 = figure(title="Ch1", height=plot_height, y_axis_type="log",
                width=900)
    p1.yaxis.axis_label = 'Voltage PSD dB(V^2/Hz)'
    p1.sizing_mode = "scale_width"

    p2 = figure(title="Ch2", height=plot_height, y_axis_type="log",
                width=900, x_range = p1.x_range)
    p2.xaxis.axis_label = 'Frequency (Hz)'
    p2.yaxis.axis_label = 'Voltage PSD dB(V^2/Hz)'
    p2.sizing_mode = "scale_width"

    l1 = p1.line(x=[0,1],y=[1,10])
    l2 = p2.line(x=[0,1],y=[1,10])

    ##########
    ## Make UI Controls
    ##########
    
    stop_button = widgets.ToggleButton(description='Stop')
    pause_button = widgets.ToggleButton(description='Pause')

    def reset_average(w):
        global n_filled, ind_next
        n_filled = 0
        ind_next = 0

    def update_freq(w):
        trace_info.value = "Frequency resolution: %.2f Hz &nbsp; &nbsp; &nbsp; &nbsp;" % (1/s.duration)
        trace_info.value += "Max frequency: %.3f kHz" % (1/s.duration*n_psd/1e3)

    decimation = widgets.Dropdown(description="Decimation:", 
                                  options=decimination_dropdown_options())
    decimation.observe(reset_average)
    decimation.observe(update_freq)
    trace_info = widgets.HTML()

    style = {'description_width': 'initial'}
    enable_average = widgets.Checkbox(description="Enable averaging (max 100 traces)", style=style)
    averaged_num = widgets.HTML(value="1", description="Number averaged:", style=style)
    reset_button = widgets.Button(description="Reset averaging")
    reset_button.on_click(reset_average)

    def save_data(w):
        global folder
        fmt = f"PSD_%Y-%m-%d-%H_%M_%S.dat"
        outname = folder + "/" + datetime.now().strftime(fmt)
        np.savetxt(outname, np.array([f,ch1_psd,ch2_psd]).T)
        filename.value = outname

    save_button = widgets.Button(description='Save data')
    save_button.on_click(save_data)
    filename = widgets.HTML(description="Last filename: ",style=style)
    filename.value = "(none)"
    
    channels = widgets.Dropdown(description='Display')
    channels.options = ["Ch1 and Ch2", "Ch1 only", "Ch2 only"]
    channels.value = "Ch1 and Ch2"
    def set_channels(w):
        if channels.index == 0:
            p1.visible = True
            p2.visible = True
        elif channels.index == 1:
            p1.visible = True
            p2.visible = False
        else:
            p1.visible = False
            p2.visible = True
        push_notebook(handle=target)
    channels.observe(set_channels)
    
    scale_height = widgets.FloatSlider(description="Plot height")
    scale_height.min = 0.2
    scale_height.max = 2
    scale_height.step = 0.1
    scale_height.value = 1
    def set_height(w):
        p1.height = int(plot_height*scale_height.value)
        p2.height = int(plot_height*scale_height.value)
        push_notebook(handle=target)
    scale_height.observe(set_height)

    ##########
    ## Initialise and show things
    ##########
    
    update_display()
    update_freq(0)
    target = show(column(p1,p2,sizing_mode='scale_width'), notebook_handle=True)
    rows = []
    rows.append(widgets.HBox([channels,scale_height]))
    rows.append(widgets.HBox([decimation,trace_info]))
    rows.append(widgets.HBox([enable_average, averaged_num, reset_button]))
    rows.append(widgets.HBox([stop_button,pause_button,save_button, filename]))
    display(widgets.VBox(rows))

    ##########
    ## The event loop
    ##########
    while True:
        with ui_events() as ui_poll:
            ui_poll(10)
        if not pause_button.value:
            update_display()
        if stop_button.value:
            break
        push_notebook(handle=target)
        # A hand coded hack to prevent bokeh from running 
        # too far behind...
        time.sleep(bokeh_sleep)

    print("Live view done")


if DEBUG:
    run_spectrum_analyzer()


# # Zoom FFT

# +
# Because of the way the Pyrpl scope module works (only two channels), we cannot use it 
# for zoom on two channels since iq2 demod that gives both quadrtures module need
# two scope channels for trace acquisition. So the ZoomFFT analyzer works only with 
# signals on Ch1

def run_spectrum_analyzer_zoomFFT():
    global folder, s, r

    # The calculated spectral densities
    psd = []
    f = []
    t = []

    # Each PSD trace is 8193*2 points long (negative freq in IQ)
    n_psd = s.data_length
    nmax  =  100
    psd_array = np.zeros([nmax,n_psd])

    # For keeping track of how many we've filled and where the next goes
    ind_next = 0
    n_filled = 0

    # Trigger needs to be "immediate"
    s.rolling_mode = False
    s.trigger_source = 'immediately'
    
    # Set up the IQ demodulator
    r.iq2.setup(
        input = 'in1', #Meaning IN1 is used
        frequency=90e3,
        bandwidth=[20e3]*2, # 4th order filter TODO let user select
        gain=0,
        phase=0,
        acbandwidth=0,
        quadrature_factor = 1, # Supremely important it does not work without does for some reason
        amplitude=1,
        output_direct='off',
        output_signal='quadrature'
    )
    r.iq2.bandwidth_options[-1]
    
    # Set up defaults
    s.input1 = 'in1'
    s.input2 = 'iq2_2'
    
    def update_psd(psd_latest):
        nonlocal psd_array, ind_next, n_filled, psd
        if enable_average.value:
            # ind_next: the running index
            # n_filled: the number of traces taken since the last reset
            psd_array[ind_next,:] = psd_latest
            if n_filled  == 0:
                psd = psd_latest
            elif n_filled < nmax:
                psd = (psd * n_filled + psd_latest) / (n_filled+1) 
            else:
                # this should work because of numpy array index wrapping
                psd += psd_latest / nmax - psd_array[ind_next-nmax]/nmax
            ind_next += 1
            if ind_next == 100:
                ind_next = 0
            if n_filled < 100:
                n_filled += 1
                averaged_num.value = "%d" % n_filled          
        else:
            psd = psd_latest
            n_filled = 0
            ind_next = 0
            if averaged_num.value != "1":
                averaged_num.value = "1"  
                
    def update_display():
        nonlocal t,f,psd
        s.decimation = decimation.value
        if zoom_enabled.value:
            s.input1 = 'iq2'
            r.iq2.frequency = zoom_freq.value
            I,Q,t = get_traces()
            # This should work? Also, need to check what
            f, psd_latest = periodogram(I+1j*Q,1/t[1],return_onesided=False)
            f = np.fft.fftshift(f)
            psd_latest = np.fft.fftshift(psd_latest)
            if zoom_rel.value:
                p.xaxis.axis_label = "Relative Frequency (Hz)"
            else: 
                p.xaxis.axis_label = "Frequency (Hz)"
                f += zoom_freq.value
        else:
            s.input1 = 'in1'
            V,_,t = get_traces()
            t = s.times
            # to deal with the fact that array is different sized...
            psd_latest = np.zeros(len(psd))
            f, psd_latest = periodogram(V,1/t[1],return_onesided=False)
            p.xaxis.axis_label = "Frequency (Hz)"
        update_psd(psd_latest)
        if zoom_enabled.value:
            l.data_source.data = dict(x=f, y=psd)
        else:
            l.data_source.data = dict(x=f[:n_psd//2], y=psd[:n_psd//2])

    ##########
    ## Make the Plots
    ##########
    
    p = figure(title="Ch1", height=400, y_axis_type="log",
                width=900)
    p.yaxis.axis_label = 'Voltage PSD dB(V^2/Hz)'
    p.sizing_mode = "scale_width"
    l = p.line(x=[0,1],y=[1,10])

    ##########
    ## Make UI Controls
    ##########
    
    stop_button = widgets.ToggleButton(description='Stop')
    pause_button = widgets.ToggleButton(description='Pause')

    def reset_average(w):
        nonlocal n_filled, ind_next
        n_filled = 0
        ind_next = 0

    def update_freq(w):
        fmt = EngFormatter()
        fmt.places = 1
        fmt.unit = "Hz"
        trace_info.value = "Frequency resolution: " + fmt(1/s.duration)
        trace_info.value += "&nbsp; &nbsp; &nbsp; &nbsp;"
        trace_info.value += "Frequency span: " + fmt(n_psd/s.duration)

    decimation = widgets.Dropdown(description="Decimation:", 
                                  options=decimination_dropdown_options())
    decimation.observe(reset_average)
    decimation.observe(update_freq)
    trace_info = widgets.HTML()

    style = {'description_width': 'initial'}
    enable_average = widgets.Checkbox(description="Enable averaging (max 100 traces)", style=style)
    averaged_num = widgets.HTML(value="1", description="Number averaged:", style=style)
    reset_button = widgets.Button(description="Reset averaging")
    reset_button.on_click(reset_average)

    def save_data(w):
        global folder
        fmt = f"PSD_zoom_%Y-%m-%d-%H_%M_%S.dat"
        outname = folder + "/" + datetime.now().strftime(fmt)
        np.savetxt(outname, np.array([f,psd]).T)
        filename.value = outname

    save_button = widgets.Button(description='Save data')
    save_button.on_click(save_data)
    filename = widgets.HTML(description="Last filename: ",style=style)
    filename.value = "(none)"
    
    zoom_enabled = widgets.Checkbox(description="Enable Zoom FFT")
    zoom_freq = widgets.FloatText(description="Freq (Hz):")
    zoom_freq.value = 98e3
    zoom_rel = widgets.Checkbox(description = "Plot relative freq")
    
    scale_height = widgets.FloatSlider(description="Plot height")
    scale_height.min = 0.2
    scale_height.max = 2
    scale_height.step = 0.1
    scale_height.value = 1
    def set_height(w):
        p.height = int(plot_height*scale_height.value)
        push_notebook(handle=target)
    scale_height.observe(set_height)
    
    ##########
    ## Initialise and show things
    ##########
    
    update_display()
    update_freq(0)
    target = show(p, notebook_handle=True)
    rows = []
    rows.append(widgets.HBox([decimation,trace_info]))
    rows.append(widgets.HBox([enable_average, averaged_num, reset_button, scale_height]))
    rows.append(widgets.HBox([zoom_enabled, zoom_freq, zoom_rel]))
    rows.append(widgets.HBox([stop_button,pause_button,save_button, filename]))
    display(widgets.VBox(rows))

    ##########
    ## The event loop
    ##########
    while True:
        with ui_events() as ui_poll:
            ui_poll(10)
        if not pause_button.value:
            update_display()
        if stop_button.value:
            break
        push_notebook(handle=target)
        time.sleep(bokeh_sleep)

    print("Live view done")


# -

if DEBUG:
    run_spectrum_analyzer_zoomFFT()


# # Oscilloscope (with generator controls)

def run_oscilloscope():
    # The traces we download
    ch1 = []
    ch2 = []
    t = []
    s.input1 = "in1"
    s.input2 = "in2"

    # For keeping track of how many we've filled and where the next goes
    ind_next = 0
    n_filled = 0

    ######
    # The acquisition and plot update
    ######
    
    def update_display():
        nonlocal ch1,ch2,t
        s.decimation = decimation.value
        ch1,ch2,t = get_traces()
        source1.data = dict(x=t, y=ch1)
        source2.data = dict(x=t, y=ch2)
   
    #####
    # Set up the plots
    #####
    
    # Not really needed now that I found the way to do it directly 
    # from the plot object
    source1 = ColumnDataSource()
    source2 = ColumnDataSource()

    p1 = figure(title="Ch1", height=300, width=900)
    p1.sizing_mode = "scale_width"
    p1.yaxis.axis_label = 'Voltage (V)'
    p1.yaxis[0].formatter = PrintfTickFormatter(format="%.2e")

    p2 = figure(title="Ch2", height=300, width=900, x_range = p1.x_range)
    p2.xaxis.axis_label = 'Time (s)'
    p2.yaxis.axis_label = 'Voltage (V)'
    p2.sizing_mode = "scale_width"
    p2.yaxis[0].formatter = PrintfTickFormatter(format="%.2e")

    p1.line('x', 'y', source=source1)
    p2.line('x', 'y', source=source2)

    #####
    # Set up the GUI
    #####
    
    stop_button = widgets.ToggleButton(description='Stop')
    pause_button = widgets.ToggleButton(description='Pause')

    def reset_average(w):
        nonlocal n_filled, ind_next
        n_filled = 0
        ind_next = 0

    def update_time(w):
        fmt = EngFormatter()
        fmt.places = 1
        fmt.unit = "s"
        trace_info.value = "Time resolution: " + fmt(s.duration/s.data_length)
        trace_info.value += "&nbsp; &nbsp; &nbsp; &nbsp;"
        trace_info.value += "Acquisition time: " + fmt(s.duration)

    decimation = widgets.Dropdown(description="Decimation:", 
                                  options=decimination_dropdown_options())
    decimation.observe(reset_average)
    decimation.observe(update_time)
    trace_info = widgets.HTML()

    def set_trigger(w):
        s.trigger_source = trigger_source.value

    trigger_source = widgets.Dropdown(description="Trigger source:", options=s.trigger_sources)
    trigger_source.observe(set_trigger)
    trigger_source.value = s.trigger_source

    trigger_threshold = widgets.FloatText(description="Trigger threshold:")
    def set_trigger_threshold(w):
        s.threshold = trigger_threshold.value
    trigger_threshold.value = s.threshold

    style = {'description_width': 'initial'}
    enable_average = widgets.Checkbox(description="Enable averaging (max 100 traces)", style=style)
    averaged_num = widgets.HTML(value="1", description="Number averaged:", style=style)
    reset_button = widgets.Button(description="Reset averaging")
    reset_button.on_click(reset_average)

    def save_data(w):
        global folder
        fmt = f"Scope_%Y-%m-%d-%H_%M_%S.dat"
        outname = folder + "/" + datetime.now().strftime(fmt)
        np.savetxt(outname, np.array([t,ch1,ch2]).T)
        filename.value = outname

    save_button = widgets.Button(description='Save data')
    save_button.on_click(save_data)
    filename = widgets.HTML(description="Last filename: ",style=style)
    filename.value = "(none)"
    
    scale_height = widgets.FloatSlider(description="Plot height")
    scale_height.min = 0.2
    scale_height.max = 2
    scale_height.step = 0.1
    scale_height.value = 1
    def set_height(w):
        p1.height = int(plot_height*scale_height.value)
        p2.height = int(plot_height*scale_height.value)
        push_notebook(handle=target)
    scale_height.observe(set_height)
    
    channels = widgets.Dropdown(description='Display')
    channels.options = ["Ch1 and Ch2", "Ch1 only", "Ch2 only"]
    channels.value = "Ch1 and Ch2"
    def set_channels(w):
        if channels.index == 0:
            p1.visible = True
            p2.visible = True
        elif channels.index == 1:
            p1.visible = True
            p2.visible = False
        else:
            p1.visible = False
            p2.visible = True
        push_notebook(handle=target)
    channels.observe(set_channels)
    
    def make_control(asg):
        freq = widgets.FloatText(description="Freq")
        freq.value = asg.frequency
        def set_freq(w):
            asg.frequency = freq.value
        freq.observe(set_freq)
        amp = widgets.FloatText(description="Amp")
        amp.value = asg.amplitude
        def set_amp(w):
            asg.amplitude = amp.value
        amp.observe(set_amp)
        off = widgets.FloatText(description="Offset")
        off.value = asg.offset
        def set_offset(w):
            asg.offset = off.value
        off.observe(set_offset)
        wfm = widgets.Dropdown(description="Wfm", options = asg.waveform_options)
        wfm.value = asg.waveform
        def set_wfm(w):
            asg.waveform = wfm.value
        wfm.observe(set_wfm)    
        return widgets.HBox([widgets.HTML(),freq, amp, off, wfm])
    
    gen0_control = make_control(r.asg0)
    gen0_control.children[0].value = "SigGen0"
    gen1_control = make_control(r.asg1)
    gen1_control.children[0].value = "SigGen1"
    
    ##########
    ## Initialise and show things
    ##########
    
    update_display()
    update_time(0)
    target = show(column(p1,p2, sizing_mode='scale_width'), notebook_handle=True)
    rows = []
    rows.append(widgets.HBox([channels,scale_height]))
    rows.append(widgets.HBox([decimation,trace_info]))
    rows.append(widgets.HBox([trigger_source,trigger_threshold]))
    #rows.append(widgets.HBox([enable_average, averaged_num, reset_button]))
    rows.append(widgets.HBox([stop_button,pause_button,save_button, filename]))
    rows.append(gen0_control)
    rows.append(gen1_control)
    display(widgets.VBox(rows))

    ##########
    ## The event loop
    ##########
    
    while True:
        with ui_events() as ui_poll:
            ui_poll(10)
        if not pause_button.value:
            update_display()
            push_notebook(handle=target)
        if stop_button.value:
            break
        time.sleep(bokeh_sleep)

    print("Live view done")


if DEBUG:
    run_oscilloscope()

# # Response analyzer

# +
raw_data_global = None
z_global = None
f_global = None
t_global = None

def run_response_analyzer():

    ######
    # Set up the plots
    ######

    target = None
    # Handy (and realisitic) data for testing plots
    f = np.linspace(1,100,100)
    z = 1/(1j*(f-50) + 5)

    def make_plot():
        nonlocal target, f, z
        # Log scale for Amplitude
        p1 = figure(height=plot_height, width=900, y_axis_type = "log")
        p1.sizing_mode = "scale_width"
        p1.xaxis.axis_label = "Frequency (Hz)"
        p1.yaxis.axis_label_text_color = 'blue'
        l1 = p1.line(f,np.abs(z), color='blue')

        # Linear scale (amp + phase)
        p2 = figure(height=plot_height, width=900)
        p2.sizing_mode = "scale_width"
        p2.xaxis.axis_label = "Frequency (Hz)"
        p2.yaxis.axis_label = "Amplitude"
        p2.yaxis.axis_label_text_color = 'blue'
        l2 = p2.line(f,np.abs(z), color='blue')
        p2.extra_y_ranges["phase"] = DataRange1d()
        ax2 = LinearAxis(y_range_name="phase", axis_label = "Phase (degrees)", 
                         axis_label_text_color = "red")
        p2.add_layout(ax2, 'right')
        l3 = p2.line(f,np.unwrap(np.angle(z))/np.pi*180, 
                     color="red", y_range_name = "phase")
        p2.y_range.renderers = [l2]

        plot_type = widgets.Dropdown(description="plot")
        plot_type.options = ["Log Amplitude", 
                             "Amplitude and Phase", 
                             "Quadratures"]

        # Quadratures (real + imag)
        p3 = figure(height=plot_height, width=900)
        p3.sizing_mode = "scale_width"
        p3.xaxis.axis_label = "Frequency (Hz)"
        p3.yaxis.axis_label = "Real Quadrature"
        p3.yaxis.axis_label_text_color = 'green'
        l4 = p3.line(f,np.real(z), color='green')
        p3.extra_y_ranges["imag"] = DataRange1d()
        ax3 = LinearAxis(y_range_name="imag", axis_label = "Imaginary Quadrature", 
                         axis_label_text_color = "purple")
        p3.add_layout(ax3, 'right')
        l5 = p3.line(f,np.imag(z), color="purple", y_range_name = "imag")

        plot_type = widgets.Dropdown(description="plot")
        plot_type.options = ["Log Amplitude",
                             "Amplitude",
                             "Amplitude and Phase", 
                             "Quadratures"]
        plot_type.value = "Amplitude"
        def set_plot_type(w):
            if plot_type.value == "Log Amplitude":
                p1.visible = True
                p2.visible = False
                p3.visible = False
            elif plot_type.value == "Amplitude":
                p1.visible = False
                p2.visible = True
                p3.visible = False
                l3.visible = False
                ax2.visible = False
            elif plot_type.value == "Amplitude and Phase":
                p1.visible = False
                p2.visible = True
                p3.visible = False
                l3.visible = True
                ax2.visible = True
            else: # plot_type.value == "Quadratures":
                p1.visible = False
                p2.visible = False
                p3.visible = True
            if target:
                push_notebook(handle=target)
        plot_type.observe(set_plot_type)
        set_plot_type([])

        def update_data():
            l1.data_source.data = dict(x=f, y=np.abs(z))
            l2.data_source.data = dict(x=f, y=np.abs(z))
            l3.data_source.data = dict(x=f, y=np.unwrap(np.angle(z)/np.pi*180))
            l4.data_source.data = dict(x=f, y=np.real(z))
            l5.data_source.data = dict(x=f, y=np.imag(z))

        return column(p1,p2,p3, sizing_mode="scale_width"), plot_type, update_data

    plot, plot_type, update_plot_data = make_plot()

    ui_rows = []
    ui_rows.append(widgets.HBox([plot_type]))

    #####
    # Measurement control widgets
    #####

    style = {'description_width': 'initial'}

    input_selection = widgets.Dropdown()
    input_selection.description = "Input"
    input_selection.options = ["in1", 'in2']
    input_selection.value = "in1"

    output_selection = widgets.Dropdown()
    output_selection.description = "Output"
    output_selection.options = ["out1", 'out2']
    output_selection.value = "out1"

    amp_out = widgets.FloatText(description = "Ampl (V)")
    amp_out.value = 0.5

    off_out = widgets.FloatText(description = "Offset (V)")
    off_out.value = 0

    ui_rows.append(widgets.HBox([input_selection, output_selection, amp_out, off_out]))

    decimation_selection = widgets.Dropdown()
    decimation_selection.description = "Time per point:"
    decimation_selection.options = decimination_dropdown_options()
    decimation_selection.value = 1
    trace_info = widgets.HTML()
    def update_info(w):
        fmt = EngFormatter()
        fmt.places = 1
        fmt.unit = "Hz"
        trace_info.value = "IF bandwidth / Freq resolution: " + fmt(1/s.duration)
    decimation_selection.observe(update_info)
    update_info([])

    ui_rows.append(widgets.HBox([decimation_selection, trace_info]))

    freq_start = widgets.BoundedFloatText(description="Start freq (Hz):")
    freq_start.min = 1
    freq_start.max = 62e6
    freq_start.value = 1e6
    freq_stop = widgets.BoundedFloatText(description="Stop freq (Hz):")
    freq_stop.min = 1
    freq_stop.max = 62e6
    freq_stop.value = 40e6
    num_pts = widgets.BoundedIntText(description = "# pts")
    num_pts.min = 2
    num_pts.max = 1e6
    num_pts.value = 102

    ui_rows.append(widgets.HBox([freq_start, freq_stop, num_pts]))
    
    def save_data(w):
        fmt = f"response_analyzer_%Y-%m-%d-%H_%M_%S.dat"
        outname = folder + "/" + datetime.now().strftime(fmt)
        np.savetxt(outname, np.array([f,np.abs(z)]).T)
        filename.value = outname

    save_button = widgets.Button(description='Save data')
    save_button.on_click(save_data)
    filename = widgets.HTML(description="Last filename: ")
    filename.value = "(none)"
    
    start_button = widgets.Button(description="Start run")
    stop_button = widgets.ToggleButton(description="Stop run")

    # Set up the IQ demodulator
    r.iq2.setup(
        input = 'in1', #Meaning IN1 is used
        frequency=90e3,
        bandwidth=[20e3]*2, # 4th order filter TODO let user select
        gain=0,
        phase=0,
        acbandwidth=0,
        quadrature_factor = 1, # Supremely important it does not work without does for some reason
        amplitude=0.9999999,
        output_direct='off',
        output_signal='quadrature')
    iq = r.iq2
    s.input1 = 'iq2'
    s.input2 = 'iq2_2'
    s.trigger_source = "immediately"
    
    r.iq2.bandwidth_options[-1]
    def do_measurement(w):
        nonlocal f,z,target
        iq.input = input_selection.value
        s.trigger_source = 'asg0'
        s.decimation = decimation_selection.value
        r.asg0.output_direct = output_selection.value
        r.asg0.amplitude = amp_out.value
        r.asg0.waveform = "cos"
        r.asg0.offset = off_out.value
        z = np.zeros(num_pts.value)+0j
        z[:] = np.nan
        f = np.linspace(freq_start.value, freq_stop.value, num_pts.value)
        for fi,i in zip(f,range(num_pts.value)):
            r.asg0.frequency = fi
            iq.frequency = fi
            real, imag, t = get_traces()
            real = np.average(real)
            imag =np.average(imag)
#             ch1 -= np.average(ch1)
#             real = np.sum(ch1*np.cos(2*np.pi*fi*t))/np.sum(np.cos(2*np.pi*fi*t)**2)
#             imag = np.sum(ch1*np.sin(2*np.pi*fi*t))/np.sum(np.sin(2*np.pi*fi*t)**2)
            z[i] = real+1j*imag
            update_plot_data()
            with ui_events() as ui_poll:
                ui_poll(10)
            if stop_button.value:
                start_button.value = False
                stop_button.value = True
                return
            push_notebook(handle=target)

    start_button.on_click(do_measurement)
    ui_rows.append(widgets.HBox([start_button, stop_button]))
    ui_rows.append(widgets.HBox([save_button, filename]))

    target = show(plot, notebook_handle=True)
    display(widgets.VBox(ui_rows))


# -

if DEBUG: 
    run_response_analyzer()


