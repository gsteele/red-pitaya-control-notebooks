---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.15.0
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

# Python RedPitaya Control

Some widgets and code for acquiring data and controlling the RedPitaya using the PyRPL library. 

You will need to install PyRPL: 

https://pyrpl.readthedocs.io/en/latest/user_guide/installation/pyrpl_installation.html#installation-from-source

From an Ananconda prompt, run the following:

```
conda install python=3.9 numpy=1.19
conda install numpy scipy paramiko pandas nose pip pyqt qtpy pyqtgraph pyyaml
conda install ipywidgets
conda install notebook=6.0
pip install pyrpl quamash
```

The following cells (including hidden one) imports the libraries and initialises everything. 


```python
#HOSTNAME = "rp-f07198.local" # hostname of the red pitaya
#HOSTNAME = "rp-f0839f.local" # hostname of the red pitaya
HOSTNAME = "rp-f071a9.local" # hostname of the red pitaya
folder = "."  # relative folder where files are saved
folder = "/Users/gsteele/Documents/GitHub/tank-circuit-readout/"
```

```python hide_input=false
import warnings
warnings.filterwarnings('ignore')

import sys
import os
from pyrpl import Pyrpl
import numpy as np
import time

import IPython
import ipywidgets as widgets

from bokeh.plotting import figure, show
from bokeh.io import output_notebook, push_notebook
from bokeh.layouts import column, row
from bokeh.models import ColumnDataSource, Toggle, Range1d

from scipy.signal import welch

from datetime import datetime

# Red Pitaya Stuff

p = Pyrpl('test', hostname=HOSTNAME, gui=False)
r = p.rp
s = r.scope

# Some good default settings
s.input1 = 'in1'
s.input2 = 'in2'
s.decimation = 1024 # or s.duration =0.01
s.average = True
s.trigger_source = 'immediately'

# For the gui updates during the live view loops
ipython = IPython.get_ipython()

# For Bokeh
output_notebook()

# The trace acquisition
def get_traces(decimation = 1024):
    s.decimation = decimation
    s._start_acquisition()
    ch1,ch2 = s._get_curve()
    t = s.times
    return ch1,ch2,t
```

<!-- #region hide_input=true -->
# Generator Control
<!-- #endregion -->

```python
sg_ch1 = r.asg0
sg_ch1.output_direct = 'out1'
sg_ch2 = r.asg1
sg_ch2.output_direct = 'out2'
```

```python
sg_ch1.waveform_options
```

```python
sg_ch1.setup(frequency=10e3, amplitude=0.5, offset=0.5, waveform='sin', trigger_source='immediately')
#sg_ch2.setup(frequency=1e6, amplitude=0.0, waveform='cos', trigger_source='immediately')
```

# Spectrum Analyser

```python hide_input=false
# The traces we download
ch1 = []
ch2 = []
t = []

# The calculated spectral densities
ch1_psd = []
ch2_psd = []
f = []

# Each PSD trace is 8193 points long
n_psd = 8193
nmax  =  100
ch1_psd_array = np.zeros([nmax,n_psd])
ch2_psd_array = np.zeros([nmax,n_psd])

# For keeping track of how many we've filled and where the next goes
ind_next = 0
n_filled = 0

# Trigger needs to be "immediate"
s.trigger_source = 'immediately'

def update_display():
    global ch1,ch2,t,f,ch1_psd,ch2_psd,last_trace_time,ind_next,n_filled
    
    t0 = time.time()
#    ch1,ch2 = s.single()
#    t = s.times
    time.sleep(s.duration)
    ch1,ch2 = s._get_curve()
    t = s.times
    t1 = time.time(); print("%.1f " % ((t1-t0)*1e3), end=''); t0 = t1

    f, ch1_psd_trace = welch(ch1,1/t[1],nperseg=len(ch1))
    f, ch2_psd_trace = welch(ch2,1/t[1],nperseg=len(ch2))
    t1 = time.time(); print("%.1f " % ((t1-t0)*1e3), end=''); t0 = t1    
    if enable_average.value:
        # ind_next: the running index
        # n_filled: the number of traces taken since the last reset
        
        ch1_psd_array[ind_next,:] = ch1_psd_trace
        ch2_psd_array[ind_next,:] = ch2_psd_trace
        
        if n_filled  == 0:
            ch1_psd = ch1_psd_trace
            ch2_psd = ch2_psd_trace
        elif n_filled < nmax:
            ch1_psd = (ch1_psd * n_filled + ch1_psd_trace) / (n_filled+1) 
            ch2_psd = (ch2_psd * n_filled + ch2_psd_trace) / (n_filled+1) 
        else:
            # this should work because of numpy array index wrapping
            ch1_psd += ch1_psd_trace / nmax - ch1_psd_array[ind_next-nmax]/nmax
            ch2_psd += ch2_psd_trace / nmax - ch2_psd_array[ind_next-nmax]/nmax  
        ind_next += 1
        if ind_next == 100:
            ind_next = 0
        if n_filled < 100:
            n_filled += 1
            averaged_num.value = "%d" % n_filled          
    else:
        ch1_psd = ch1_psd_trace
        ch2_psd = ch2_psd_trace
        n_filled = 0
        ind_next = 0
        if averaged_num.value != "1":
            averaged_num.value = "1"
    t1 = time.time(); print("%.1f " % ((t1-t0)*1e3), end=''); t0 = t1
    print("Updating data sources!")
    l1.data_source.data = dict(x=f, y=ch1_psd)
    l2.data_source.data = dict(x=f, y=ch2_psd)
    t1 = time.time(); print("%.1f " % ((t1-t0)*1e3), end=''); t0 = t1
    print("           \r", end='')

p1 = figure(title="Ch1", height=300, y_axis_type="log",
            width=900)
p1.yaxis.axis_label = 'Voltage PSD dB(V^2/Hz)'
p1.sizing_mode = "scale_width"

p2 = figure(title="Ch2", height=300, y_axis_type="log",
            width=900, x_range = p1.x_range)
p2.xaxis.axis_label = 'Frequency (Hz)'
p2.yaxis.axis_label = 'Voltage PSD dB(V^2/Hz)'
p2.sizing_mode = "scale_width"

l1 = p1.line(x=[0,1],y=[1,10])
l2 = p2.line(x=[0,1],y=[1,10])

stop_button = widgets.ToggleButton(description='Stop')
pause_button = widgets.ToggleButton(description='Pause')

def reset_average(w):
    global n_filled, ind_next
    n_filled = 0
    ind_next = 0

# For the decimation GUI
options = []
for d,t in zip(s.decimation_options, s.duration_options):
    fmax = (1/t*n_psd/1e3)
    if t < 1e-3:
        t*= 1e6
        u = "us"
    elif t<1:
        t*= 1e3
        u = "ms"
    else: 
        u = "s"
    options.append((str(d) + ", " "{0:.3g}".format(t) + " " + u + ", %.1f kHz" % fmax,d))
options

def update_freq(w):
    s.decimation = decimation.value
    trace_info.value = "Frequency resolution: %.2f Hz &nbsp; &nbsp; &nbsp; &nbsp;" % (1/s.duration)
    trace_info.value += "Max frequency: %.3f kHz" % (1/s.duration*n_psd/1e3)

decimation = widgets.Dropdown(description="Decimation:", 
                              options=options)
decimation.observe(reset_average)
decimation.observe(update_freq)
trace_info = widgets.HTML()

style = {'description_width': 'initial'}
enable_average = widgets.Checkbox(description="Enable averaging (max 100 traces)", style=style)
averaged_num = widgets.HTML(value="1", description="Number averaged:", style=style)
reset_button = widgets.Button(description="Reset averaging")
reset_button.on_click(reset_average)

def save_data(w):
    fmt = f"PSD_%Y-%m-%d-%H_%M_%S.dat"
    outname = folder + "/" + datetime.now().strftime(fmt)
    np.savetxt(outname, np.array([f,ch1_psd,ch2_psd]).T)
    filename.value = outname
    
save_button = widgets.Button(description='Save data')
save_button.on_click(save_data)
filename = widgets.HTML(description="Last filename: ",style=style)
filename.value = "(none)"

update_display()
update_freq(0)
target = show(column(p1,p2,sizing_mode='scale_width'), notebook_handle=True)
rows = []
rows.append(widgets.HBox([decimation,trace_info]))
rows.append(widgets.HBox([enable_average, averaged_num, reset_button]))
rows.append(widgets.HBox([stop_button,pause_button,save_button, filename]))
display(widgets.VBox(rows))

while True:
    ipython.kernel.do_one_iteration()
    if not pause_button.value:
        update_display()
    ipython.kernel.do_one_iteration()
    if stop_button.value:
        break
    push_notebook(handle=target)
    time.sleep(0.1)
    break

print("Live view done")
```

# Oscilloscope

```python hide_input=false
# The traces we download
ch1 = []
ch2 = []
t = []

ch1,ch2 = s.single()

# For keeping track of how many we've filled and where the next goes
ind_next = 0
n_filled = 0

# For the scope, we will actually use an "rolling mode" acquisition
s._start_acquisition_rolling_mode()
s.ch1_active = True
s.ch2_active = True

source1 = ColumnDataSource()
source2 = ColumnDataSource()

p1 = figure(title="Ch1", height=300, width=900)
p1.yaxis.axis_label = 'Voltage (V)'
p1.sizing_mode = "scale_width"


p2 = figure(title="Ch2", height=300, width=900, x_range = p1.x_range)
p2.xaxis.axis_label = 'Time (s)'
p2.yaxis.axis_label = 'Voltage (V)'
p2.sizing_mode = "scale_width"

p1.line('x', 'y', source=source1)
p2.line('x', 'y', source=source2)

stop_button = widgets.ToggleButton(description='Stop')
pause_button = widgets.ToggleButton(description='Pause')

def reset_average(w):
    global n_filled, ind_next
    n_filled = 0
    ind_next = 0

# For the decimation GUI
options = []
try:
    dur = s.duration_options
except:
    dur = s.durations
        
for d,t in zip(s.decimation_options, dur):
    if t < 1e-3:
        t*= 1e6
        u = "us"
    elif t<1:
        t*= 1e3
        u = "ms"
    else: 
        u = "s"
    options.append((str(d) + ", " "{0:.3g}".format(t) + " " + u,d))

def update_decimation(w):
    s.decimation = decimation.value
    trace_info.value = "Time resolution: %.3e s &nbsp; &nbsp; &nbsp; &nbsp;" % (s.duration/len(ch1))
    trace_info.value += "Acquisition time: %.3e s" % (s.duration)

decimation = widgets.Dropdown(description="Decimation:", 
                              options=options)
decimation.observe(reset_average)
decimation.observe(update_decimation)
trace_info = widgets.HTML()

def set_trigger(w):
    s.trigger_source = trigger_source.value

trigger_source = widgets.Dropdown(description="Trigger source:", options=s.trigger_sources)
trigger_source.observe(set_trigger)
trigger_source.value = s.trigger_source

trigger_threshold = widgets.FloatText(description="Trigger threshold:")
def set_trigger_threshold(w):
    s.threshold = trigger_threshold.value
trigger_threshold.value = s.threshold

style = {'description_width': 'initial'}
enable_average = widgets.Checkbox(description="Enable averaging (max 100 traces)", style=style)
averaged_num = widgets.HTML(value="1", description="Number averaged:", style=style)
reset_button = widgets.Button(description="Reset averaging")
reset_button.on_click(reset_average)

npts = widgets.RadioButtons(options=[2**14, 2**12, 2**10], description="Num Points:")

def save_data(w):
    fmt = f"Scope_%Y-%m-%d-%H_%M_%S.dat"
    outname = folder + "/" + datetime.now().strftime(fmt)
    np.savetxt(outname, np.array([t,ch1,ch2]).T)
    filename.value = outname

save_button = widgets.Button(description='Save data')
save_button.on_click(save_data)
filename = widgets.HTML(description="Last filename: ",style=style)
filename.value = "(none)"

source1.data = dict(x=[0,1], y=[0,1])
source2.data = dict(x=[0,1], y=[0,1])

update_decimation(0)
target = show(column(p1,p2,sizing_mode = 'scale_width'), notebook_handle=True)
rows = []
rows.append(npts)
rows.append(widgets.HBox([decimation,trace_info]))
#rows.append(widgets.HBox([zoom,center]))
#rows.append(widgets.HBox([trigger_source,trigger_threshold]))
#rows.append(widgets.HBox([enable_average, averaged_num, reset_button]))
rows.append(widgets.HBox([stop_button,pause_button,save_button, filename]))
display(widgets.VBox(rows))

def check_gui():
    for i in range(5):
        ipython.kernel.do_one_iteration()   

while True:
    if stop_button.value:
        break
    timing = []
    timing.append(time.time())
    check_gui()
    # Data acquisition
    #ch1,ch2 = s._get_trace()
    if decimation.value < 1024:
        ch1,ch2 = s.single()
    t,(ch1,ch2) = s._get_rolling_curve()
    if npts.value != 16384:
        t = t[0:n_short]
        ch1 = ch1[-n_short:]
        ch2 = ch2[-n_short:]
    #t = s.times
    timing.append(time.time())
    check_gui()
    # Bokeh update arrays
    if not pause_button.value:
        source1.data = dict(x=t, y=ch1)
        source2.data = dict(x=t, y=ch2)
    timing.append(time.time())
    check_gui()
    # Bokeh push notebook
    if not pause_button.value:
        push_notebook(handle=target)
    timing.append(time.time())
    check_gui()
    print(np.diff(np.array(timing)*1e3), " "*10, end="\r")
    time.sleep(0.100)

print("\nLive view done")
```

Data length does not work like you think it should for rolling mode: it seems to return just a fixed range of the trace, not the most recent `data_length` number of points...

```python
s.data_length = 1000
```

```python
s.data_length
```

# Live Voltmeter / Logger

```python hide_input=true
t0 = time.time()
v1_trace = []
v2_trace = []
t_trace = []
s._start_acquisition_rolling_mode()
#s.trigger_source = "immediately"
s.decimation = 1024

source1 = ColumnDataSource()
source2 = ColumnDataSource()
p1 = figure(title="Ch1", plot_height=200,
            plot_width=900)
p1.yaxis.axis_label = 'Voltage (mV)'
p1.line('x', 'y', source=source1)

p2 = figure(title="Ch2", plot_height=200,
            plot_width=900)
p2.xaxis.axis_label = 'Time (s)'
p2.yaxis.axis_label = 'Voltage (mV)'
p2.line('x', 'y', source=source2)

stop_button = widgets.ToggleButton(description='Stop')
pause_button = widgets.ToggleButton(description='Pause')

reset_button = widgets.Button(description='Reset')
def reset(w):
    global v1_trace, v2_trace, t_trace
    v1_trace = []
    v2_trace = []
    t_trace = []
    global t0
    t0 = time.time()
reset_button.on_click(reset)

def save_data(w):
    fmt = f"Voltmeter_%Y-%m-%d-%H_%M_%S.dat"
    outname = folder + "/" + datetime.now().strftime(fmt)
    np.savetxt(outname, np.array([t_trace,v1_trace,v2_trace]).T)
    filename.value = outname

def update_display(v1, v2):
    #formatter = mpl.ticker.EngFormatter(unit="V",places=2)
    #v1_string = formatter(v1)
    #v2_string = formatter(v2)
    v1_string = "% 8.2f mV" % (v1)
    v2_string = "% 8.2f mV" % (v2)
    tmp = "<h1 style='font-size:65px;font-family:courier' align='center'><pre>" 
    tmp += v1_string + " " + v2_string + "</pre></h1>"
    display_widget.value = tmp
    
display_widget = widgets.HTML()

# For the decimation GUI
options = []
try:
    dur = s.duration_options
except:
    dur = s.durations
        
for d,t in zip(s.decimation_options, dur):
    if t < 1e-3:
        t*= 1e6
        u = "us"
    elif t<1:
        t*= 1e3
        u = "ms"
    else: 
        u = "s"
    options.append((str(d) + ", " "{0:.3g}".format(t) + " " + u,d))

decimation = widgets.Dropdown(description="Decimation:", 
                              options=options)  
decimation.value = 128

def update_decimation(w):
    s.decimation = decimation.value
decimation.observe(update_decimation)

delta_t = widgets.Text(description = "Last dt (ms):")

def update_gui():
    #ch1,ch2,t = get_traces(decimation=decimation.value)
    ch1,ch2 = s._get_trace()
    v1 = np.average(ch1)*1000
    v2 = np.average(ch2)*1000 
    update_display(v1,v2)
    v1_trace.append(v1)
    v2_trace.append(v2)
    t_trace.append(time.time()-t0)
    source1.data = dict(x=t_trace, y=v1_trace)
    source2.data = dict(x=t_trace, y=v2_trace)
    if len(t_trace) > 1:
        delta_t.value = "%.2f" % ((t_trace[-1]-t_trace[-2])*1000)
    
style = {'description_width': 'initial'}
save_button = widgets.Button(description='Save data')
save_button.on_click(save_data)
filename = widgets.HTML(description="Last filename: ",style=style)
filename.value = "(none)"

update_gui()

target = show(column(p1,p2), notebook_handle=True)
display(display_widget)
rows = []
rows.append(widgets.HBox([decimation, delta_t]))
#rows.append(widgets.HBox([zoom,center]))
#rows.append(widgets.HBox([trigger_source,trigger_threshold]))
#rows.append(widgets.HBox([enable_average, averaged_num, reset_button]))
rows.append(widgets.HBox([stop_button,reset_button,pause_button,save_button, filename]))
display(widgets.VBox(rows))

while True:
    ipython.kernel.do_one_iteration()
    if not pause_button.value:
        update_gui()
    ipython.kernel.do_one_iteration()
    if stop_button.value:
        break
    push_notebook(handle=target)
    time.sleep(0.1)
```

# Network Analyzer
