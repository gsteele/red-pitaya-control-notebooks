---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.15.1
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

# Jupyter RedPitaya Control 2023

Some widgets and code for acquiring data and controlling the RedPitaya using the PyRPL library in a Jupyter Notebook. 

The library works only with specific versions of other libraries. It is best therefore to install it in it's own environment. Gary spent a lot of time debugging this, and the following should work and get you "somewhat" recent copies of libraries:

```
conda create -n pyrpl_env
conda activate pyrpl_env
conda install python=3.8 numpy=1.19 matplotlib numpy scipy paramiko pandas nose pip pyqt qtpy pyqtgraph=0.12 pyyaml ipywidgets bokeh notebook=6.5.5
pip install pyrpl quamash jupyter-ui-poll
```

Then, start a jupyter notebook server in that environment, and things *should* work. Also, if you want to push stuff back to the git repo eventually, please install jupytext (`conda install jupytext` and `jupyter serverextension enable jupytext`) since these notebooks are paired with markdown for better interoperability with git.

```python
from rp_control_widgets import *
```

```python
HOSTNAME = "rp-f071a9.local" # hostname of the red pitaya
folder = "."  # folder where files are saved
initialize(HOSTNAME, folder)
```

# Two Channel Oscilloscope with Generator Control

```python
run_oscilloscope()
```

# Two Channel Spectrum Analyser

```python hide_input=true
run_spectrum_analyzer()
```

# Single Channel Spectrum Analyzer with Zoom FFT

```python
run_spectrum_analyzer_zoomFFT()
```

# Response analyzer

```python
run_response_analyzer()
```
